/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AIplayer.java                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 15:52:14 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 10:27:05 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * AIplayer class
 *
 * This class is part of the Abysmal game. The goal of this class is to make a
 * generic AI player class. This class is composed name, description, both
 * String Formatted
 * @author Rayan Fraj
 * @author Rakib Hernandez SHEIKH
 */
public class AIplayer {
  private String name;
  private String description;
  int health;
  int armor;

  /**
   * Default constructor of the class. n and d are String formatted. Name and
   * description respectively
   * @param n String formatted name
   * @param d String formatted description
   */
  public AIplayer(String n, String d) {
    this.name = n;
    this.description = d;
  }

  /**
   * Return the name of the AI player
   * @return String formated name
   */
  /*@
    @ pure
    @*/
  public String getName() {
    return this.name;
  }

  /**
   * Return the description of the AI Player
   * @return String formatted description
   */
  /*@
    @ pure
    @*/
  public String getDescription() {
    return this.description;
  }

  /**
    * Print he's name + description
    * @return String formated class
    */
  /*@
    @ pures
    @*/
  @Override
  public String toString () {
    return this.getName() + " " + this.getDescription();
  }

  /**
   * [receiveDamage description]
   * @param o [description]
   */
  public void receiveDamage(Weapons o) {
    int trueTaken = o.trueDamage();
    int negation = this.armor - o.reduceDamage(trueTaken);
    System.out.println("Damage taken : " + negation);
    this.health -= negation;
  }
}
