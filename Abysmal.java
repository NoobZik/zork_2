/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Abysmal.java                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 21:09:55 by NoobZik           #+#    #+#             */
/*   Updated: 2018/09/30 21:16:43 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


import java.util.*;

public class Abysmal {

    public static void main (String args[]) {
		Game leJeu;
		
		leJeu = new Game();
		leJeu.play();
    }
}
