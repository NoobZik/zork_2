/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ArrayListContents.java                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 18:40:40 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 19:55:04 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.ArrayList;
import java.util.Objects;

/**
 * Served as a One Place location of any definition of an ArrayList of ObjetZork
 * In order to limit duplicate methodes.
 *
 * @since b350d9165366c9772eb03e12a3037e9a6cf67152
 * @author Rakib Hernandez Sheikh NoobZik
 * @author Rayan Fraj
 */
public abstract class ArrayListContents {
  protected ArrayList<ObjetZork> objects;

  /**
   * Default Constructor
   */
  public ArrayListContents() {
    this.objects = new ArrayList<ObjetZork>();
  }

  /**
   * Customized Constructor with a provided ArrayList of ObjetZork
   * Acting as a clone
   *
   * @param l ArrayList of ObjetZork
   */
  public ArrayListContents(ArrayList<ObjetZork> l) {
    this.objects = new ArrayList<ObjetZork>(l);
  }

  /**
   * Check if the object given as a parameter contains to the arraylist of the
   * room
   * @param    o  Object to check in arraylist
   * @return      boolean state if it contains or not
   */
  /*@
    @ ensures \result = this.objects.contains(o);
    @*/
  public  boolean contains(ObjetZork o) {
    return this.objects.contains(o);
  }

  /**
   * Abstract add methode, see the following files
   * - Player.java
   * - Room.java
   * @param o an ObjectZork to add into the ArrayList
   * @return boolean true if successful, false overwise
   */
  public  abstract  boolean  add(ObjetZork o);

  /**
   * Abstract remove methode, see the following files - Player.java - Room.java
   *
   * @param o an ObjectZork to remove from the ArrayList
   * @return boolean true if successful, false overwise
   */
  public  abstract  boolean remove(ObjetZork o);

  /**
   * Return the occurence of a given object of the ArrayList
   * @param    o  Object occurence to get
   * @return      Integer of the occurence
   */
  /*@
    @ requires !(o.equals(null));
    @*/
  public  int howManyOf(ObjetZork o) {
    int i = -1;
    int r = 0;
    while (++i< getSize()) {
      if(objects.get(i).equals(o))
        r++;
      continue;
    }
    return r;
  }

  /**
   * Return the item from the given position to the ArrayList of ObjetZork
   *
   * @param    i  Position of a potential item
   * @return      An ObjectZork reference or null
   */
  /*@
    @ \result = object.get(i) || null;
    @*/
  public ObjetZork getObject(int i) {
    return objects.get(i);
  }

  /**
   * Return the size of the ArrayList
   * @return int formatted size of the ArratList
   */
  /*@
    @ pure
    @*/
  public int getSize() {
    return objects.size();
  }


  /**
   * Customized toString methode according to the current class
   */
/*  @Override
  public int hashcode() {
    return Objects.hash(objects,size);
  }*/
}
