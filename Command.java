/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Command.java                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 21:04:09 by NoobZik           #+#    #+#             */
/*   Updated: 2018/09/30 21:04:10 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 *
 * This class holds information about a command that was issued by the user.
 * A command currently consists of two strings: a command word and a second
 * word (for example, if the command was "take map", then the two strings
 * obviously are "take" and "map").
 *
 * The way this is used is: Commands are already checked for being valid
 * command words. If the user entered an invalid command (a word that is not
 * known) then the command word is null.
 *
 * If the command had only one word, then the second word is null.
 *
 * The second word is not checked at the moment. It can be anything. If this
 * game is extended to deal with items, then the second part of the command
 * should probably be changed to be an item rather than a String.
 *
 * @author     Michael Kolling
 * @author     Rakib Hernandez Sheikh NoobZik
 * @author     Rayan Fraj
 * @version    1.0
 * @since      ebe7515eb39df3f143703e2453d2761f45704741
 */

public class Command {
  private String commandWord;
  private String secondWord;
  private String thirdWord;

  /**
   * Create a command object. First and second word must be supplied, but
   * either one (or both) can be null. The command word should be null to
   * indicate that this was a command that is not recognised by this game.
   *
   * @param  commandWord  Le mot commande de la commande utilisateur ou null
   * @param  secondWord    Le second mot de la commande utilisateur ou null
   */
  public Command(String commandWord, String secondWord, String thirdWord) {
    this.commandWord = commandWord;
    this.secondWord = secondWord;
    this.thirdWord = thirdWord;
  }


  /**
   * Return the command word (the first word) of this command. If the
   * command was not understood, the result is null.
   *
   * @return    Le mot commande de cette Command ou null
   */
  public String getCommandWord() {
    return commandWord;
  }


  /**
   * Return the second word of this command. Returns null if there was no
   * second word.
   *
   * @return    le second mot de cette Command ou null
   */
  public String getSecondWord() {
    return secondWord;
  }

  /**
   * Return the third word of this command. Returns null if there was no
   * second word.
   *
   * @return    le second mot de cette Command ou null
   */
  public String getThirdWord() {
    return thirdWord;
  }


  /**
   *  Return true if this command was not understood.
   *
   * @return    true si cette commande est valide ; false sinon
   */
  public boolean isUnknown() {
    return (commandWord == null);
  }


  /**
   *  Return true if the command has a second word.
   *
   * @return    true si cette commande possède un second mot ; false sinon
   */
  public boolean aSecondWord() {
    return (secondWord != null);
  }

  /**
   *  Return true if the command has a third word.
   *
   * @return    true si cette commande possède un second mot ; false sinon
   */
  public boolean aThirdWord() {
    return (thirdWord != null);
  }
}
