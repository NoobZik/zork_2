/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CommandWords.java                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 20:42:41 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/29 09:35:22 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 *  <p>
 *
 * This class holds an enumeration of all command words known to the game.
 * It is used to recognise commands as they are typed in.
 *
 * This class is part of the "Zork" game.
 *
 * @author     Michael Kolling
 * @author     Rakib Hernandez Sheikh NoobZik
 * @author     Rayan Fraj
 * @version    1.0
 * @since      3a31ba4b59e9c5d4362d43e261eca6561c962f3f
 */

public class CommandWords {
  /**
   *  a constant array that holds all valid command words
   */
  private final static String validCommand[] = {
    "help",
    "open",
    "go",
    "exit",
    "return",
    "money",
    "bag",
    "take",
    "drop",
    "ls",
    "info",
    "current",
    "mission",
    "deposite",
    "wait",
    "show",
    "valider",
    "close"};


  /**
   *  Constructor - initialise the command words.
   */
  public CommandWords() { }


  /**
   * Check whether a given String is a valid command word.
   * Return true if it is, false if it isn't.
   *
   * @param  aString  Chaine de caractères a tester
   * @return          true si le paramètre est une commande valide, false sinon
   */
  public boolean isCommand(String aString) {
    for (int i = 0; i < validCommand.length; i++) {
      if (validCommand[i].equals(aString)) {
        return true;
      }
    }
    return false;
  }


  /**
   *  Print all valid commands to System.out.
   */
  public void showAll() {
    for (int i = 0; i < validCommand.length; i++) {
      System.out.print(validCommand[i] + "  ");
    }
    System.out.println();
  }
}
