/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ControlOfficer.java                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/17 16:01:34 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 10:26:25 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * ControlOfficer class
 *
 * This class is part of the Abysmal game. It is an extension of the AIPlayer.
 * The goal of this class is to make a Control Officer as a AI which it can ask
 * the player to show their navigo card. This is a enforcement rule to validate
 * transportation card.
 *
 * @author Rakib Hernandez SHEIKH (Only Correction)
 * @author Rayan Fraj
 * @since ddbc98610d47e1e7dcc6fae4a60549dccac7d710
 */

public class ControlOfficer extends AIplayer {
  private int plateNumber;
  private boolean checked = false;
  /**
   * Default constructor for the Control officer
   */
  public ControlOfficer() {
    super("Control officer", "This guy is in charge of checking metro tickets");
    plateNumber = 5;
  }

  /**
   * Return the licence plate number of the control officer
   * @return Integer formatted number
   */
  /*@
    @ pure
    @*/
  public int getPlateNumber() {
    return this.plateNumber;
  }

  /**
   * Check the current boolean state of the navigo card
   * @param c A navigo card
   * @return
   */
  /*@
    @ pure
    @*/
  private boolean checkValidation(Navigo c) {
    if(c.getValidation())
      return true;
    else return false;
  }

  /**
   * According to the current boolean statement, set the situation.
   * 1 for a navigo card already validated.
   * 2 for a navigo card not validated BUT the fine can be payed.
   * 3 A pursuit just beggun for illegaly taking transport and
   * @param p Player in argument to check the validation
   * @return 1 if validated, 2 if not but with enought money, 3 if not.
   */
  /*@
    @ requires !(p.equals(null));
    @*/
  public int isValidated(Player p) {
    checked = true;
    if (checkValidation(p.getNavigo())) {
      System.out.println("Vous êtes en règle, bon voyage");
      return 1;
    }
    else if (p.getMoney() >= 50) {
      System.out.println("Vous n'êtes pas en règle, vous disposez des fonds pour"+
      " payer immédiatement. Vous avez été débité de 50 euros");
      p.setMoney(-50);
      p.getNavigo().setValidation(true);
      return 2;
    }
    else {
      System.out.println("Vous n'êtes pas en règle !");
      System.out.print("Ammende non payé pour manque de fonds, direction");
      System.out.printf(" le tribunal de Bobigny\n");
      return 3;
    }
  }

  /**
   * Print a welcome message for the player.
   * Should be used only at the same room of the player.
   */
  /*@
    @ pure
    @*/
  public void greetPlayer() {
    System.out.println("Controleur SNCF bonjour, MERCI DE PRESENTER UN "+
    "TITRE DE TRANSPORT S'IL VOUS PLAIT");
  }

  /**
   * reset the spam protection
   */
  public void setChecked() {
    checked = false;
  }

  /**
   * Retrive the value of the spam protection
   * @return boolean statemnnt
   */
  public boolean getChecked() {
    return checked;
  }
}
