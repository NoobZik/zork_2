/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Game.java                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 19:46:58 by NoobZik           #+#    #+#             */
/*   Updated: 2018/11/14 20:36:16 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.ArrayList;
/**
 *
 * Class Game - the main class of the "Zork" game.
 *
 * This class is the main class of the "Zork" application. Zork is a very
 * simple, text based adventure game. Users can walk around some scenery. That's
 * all. It should really be extended to make it more interesting!
 *
 * To play this game, create an instance of this class and call the "play"
 * routine.
 *
 * This main class creates and initialises all the others: it creates all rooms,
 * creates the parser and starts the game. It also evaluates the commands that
 * the parser returns..
 *
 * @author     Michael Kolling
 * @author     Rakib Hernandez Sheikh NoobZik
 * @author     Rayan Fraj
 * @version    1.1
 * @since      5471c902f6d29b4217f967936df7547a95010a31
 */

public class Game {
  private Parser  parser;
  private Room    currentRoom;
  private Room    lastRoom;
  private ArrayList<ObjetZork> objectList;
  private ArrayList<Room> roomList;
  private Player  player;
  private ControlOfficer controleur = new ControlOfficer();

  private int     win      = randomWithRange(10,55);
  private int     counter  = 0;
  private boolean winGame  = false;
  private ObjetZork carteEtudiante = new ObjetZork("CarteEtudiante", "Pass-pass fac", 0, true);

  Room hallC;
  Room secretary;
  Room corridorGalilee;
  Room b303;
  Room amphiA;
  Room amphiB;
  Room amphiC;
  Room corridorAmphi;
  Room corridorC;
  Room bde;
  Room fRoom;

  // Bobigny

  Room illustration;
  Room corridorAmphiIllustration;
  Room arendt;
  Room escarpit;
  Room gutenberg;

  // Stations

  Room villetaneuseUniversite;
  Room leBourget;
  Room drancyAvenir;
  Room maison;

  Room teleportation;

  /* Tramway */

  Room tt;

  Room lastTT;
  Room currentTT;
  /**
   *  Create the game and initialise its internal map.
   */
  public Game() {
    initRooms();
    objectList = new ArrayList<ObjetZork>();
    initObjects();
    randomPieceObjectInsert(roomList, objectList);
    parser = new Parser();
    player = new Player("NoobZik");
  }

  /**
   * Create a finite objects for the game
   */
  public void initObjects() {

    /* Objets normaux 18 (0-17)*/

    objectList.add(new ObjetZork("Micro", "Fait pour parler plus fort sans forcer",1,true));
    objectList.add(new ObjetZork("Retro-Projecteur", "Appareil pour projeter un ecran de PC",5,true));
    objectList.add(new ObjetZork("Tableau", "Un tableau murale pour écrire",30,false));
    objectList.add(new ObjetZork("diskPoids", "Un diskPoids transportable de la fac", 50, true));
    objectList.add(new ObjetZork("Cours", "Si tu perd, au revoir la p2", 10, true));
    objectList.add(new ObjetZork("Chaise", "Juste des chaise bien rangé", 900, false));
    objectList.add(new ObjetZork("Voitures", "voitures qui prennent trop de place", 900, false));
    objectList.add(new ObjetZork("Haribo", "Cest des dragibus miskine", 2, true));
    objectList.add(new ObjetZork("Velleda", "Une cargaison de 100 feutres", 20, true));
    objectList.add(new ObjetZork("Cannette1", "Coca Cola", 2, true));
    objectList.add(new ObjetZork("Cannette2", "Lipton Iced tea", 2, true));
    objectList.add(new ObjetZork("Livre", "Un livre qui appartient à la BU JD", 2, true));
    objectList.add(new ObjetZork("Table", "une table fait pour écrire, ou dormir", 20, false));
    objectList.add(new ObjetZork("Katana", "Selon la légende, ce katana est perdu au sein de la fac", 20, true));
    objectList.add(new ObjetZork("PC", "L'Unité centrale de l'ordinateur", 10, true));
    objectList.add(new ObjetZork("EcranPC", "Un ecran d'un ordinateur", 5, true));
    objectList.add(new ObjetZork("SourisPC", "Une souris banale d'un ordinateur", 2, true));
    objectList.add(new ObjetZork("Clavier", "un clavier d'un ordinateur", 2, true));

    /* Armes (18-22 not used)*/

    objectList.add(new Weapons("Katana","DMG : 200, Bonus 50%", 200, 50,0));
    objectList.add(new Weapons("Epée longue", "Une épée longue légendaire de Jetch DMG : 5", 5, 0, 0));
    objectList.add(new Weapons("Epée de fraternité", "Une épée qui appartenais à Chappu DMG : 15 Bonus : 20%", 15, 20,0));
    objectList.add(new Weapons("Caladbolg", "Une épée qui a appartenu à Tidus avant qu'il soit porté disparue(DMG:150+200%)", 150, 200, 0));

    objectList.add(new Weapons("Bague de doran", "Starter pack armure, AR:100+20%", 0, 20, 100));
    objectList.add(new Weapons("Warmog", "Une armure vivante regénérative AR : 1000 + 10pv par tour", 0, 0, 1000));
    objectList.add(new Weapons("Rudaan", "Devenez une montagne de Malphite avec AR 200", 0, 0, 200));
    objectList.add(new Weapons("Titanhydra", "Une armumre random", 0, 0, 150));
  }

  /**
   *  Main play routine. Loops until end of play..
   */
  public void initRooms() {

    roomList = new ArrayList<Room>();

    // Villetaneuse



    // Room creation
    hallC = new Room("Hall C");
    corridorGalilee = new Room("Galilee corridor");
    corridorAmphi = new Room("Amphi corridor");
    corridorC = new Room("C corridor");
    b303 = new Room("B303", true);
    amphiA = new Room("Amphi A", true);
    amphiB = new Room("Amphi B", true);
    amphiC = new Room("Amphi C", true);
    bde = new Room("BDE");
    fRoom = new Room("F004-F002", true);
    secretary = new Room("secretary");

    illustration = new Room("Illustration - Bobigny");
    corridorAmphiIllustration = new Room("Couloir Illustration");
    arendt = new Room("Amphi Hanna-Arendt", false);
    escarpit = new Room("Amphi Robert-Escarpit", false);
    gutenberg = new Room("Amphi Joannes-Gutenberg", true);

    villetaneuseUniversite = new Room("Villetaneuse - Universite");
    leBourget = new Room("Le Bourget");
    drancyAvenir = new Room("Drancy - Avenir");
    maison = new Room("Maison");

    teleportation = new Room("teleportation");

    tt = new Room("Tramway TT");

    tt.setExits(drancyAvenir, null, null, null);
    villetaneuseUniversite.setExits(corridorGalilee, null,   null, null);
    leBourget.setExits(maison, null,   null, null);
    drancyAvenir.setExits(illustration, null,   null, null);

    /* Corridors linking */
    /* setSorties(North, West, South, East) */
    corridorGalilee.setExits(hallC, corridorAmphi, villetaneuseUniversite, bde);
    hallC.setExits(secretary, null, corridorGalilee, corridorC);
    corridorAmphi.setExits(amphiA, amphiB, amphiC, corridorGalilee);
    corridorC.setExits(b303, hallC, fRoom, null);
    corridorAmphiIllustration.setExits(gutenberg, arendt, illustration, escarpit);

    /* Hall C Room linking */
    secretary.setExits(null, null, hallC, null);

    /* Amphi Room linking */
    amphiA.setExits(null, null, corridorAmphi, null);
    amphiB.setExits(null, null, null, corridorAmphi);
    amphiC.setExits(corridorAmphi, null, null, null);

    arendt.setExits(null,null,null,corridorAmphiIllustration);
    escarpit.setExits(null,corridorAmphiIllustration,null,null);
    gutenberg.setExits(null,null,corridorAmphiIllustration,null);

    /* corridor C room linking */
    b303.setExits(null,null,corridorC,null);
    fRoom.setExits(corridorC,null,null,null);

    bde.setExits(null, corridorGalilee, null, null);
    illustration.setExits(corridorAmphiIllustration,null,drancyAvenir,null);

    maison.setExits(teleportation,null,leBourget,null);

    /* Adding all room into the ArrayList */

    roomList.add(hallC);
    roomList.add(corridorGalilee);
    roomList.add(b303);
    roomList.add(amphiA);
    roomList.add(amphiB);
    roomList.add(amphiC);
    roomList.add(corridorAmphi);
    roomList.add(corridorC);
    roomList.add(bde);
    roomList.add(fRoom);
    roomList.add(illustration);
    roomList.add(corridorAmphiIllustration);
    roomList.add(arendt);
    roomList.add(escarpit);
    roomList.add(gutenberg);
    roomList.add(secretary);

    maison.add(carteEtudiante);
    // Room starting point
    currentRoom = illustration;
    tt.addControl(controleur);

  }


  /**
   *  Print out the opening message for the player.
   */
  public void play() {
    printWelcome();
    boolean finished = false;
    while (!finished) {
      if (this.currentRoom.containsControl() && currentRoom.equals(tt)
            && !controleur.getChecked()) {
        System.out.println("Controleur SNCF, présentez titre de transport svp");
        System.out.println("(Il faudra tapper : montrer navigo SNCF)");
      }
      Command command = parser.getCommand();
      finished = processCommand(command);
      checkControl();
    }
    System.out.println("Thank you for playing, farewell.");
  }

  /**

   *  Print out the opening message for the player.
   */
  public void printWelcome() {
    System.out.println();
    System.out.println("Welcome to the Abysmal World!");
    System.out.println("Type help to discover your mission");
    System.out.println();
    System.out.println(currentRoom.longDescription());
  }




  /**
   *  Given a command, process (that is: execute) the command. If this command
   *  ends the game,
   *
   * @param  command  Command to process
   * @return          true is returned, otherwise false is returned.
   */
  public boolean processCommand(Command command) {

    /* Unknown */

    if (command.isUnknown()) {
      System.out.println("Command not found");
      return false;
    }

    String motCommande = command.getCommandWord();

    /* Help */

    if (motCommande.equals("help")) {
      printHelp();
    }

    /* Go */

    else if (motCommande.equals("go")) {
      goRoom(command);
    }

    /* Exit */

    else if (motCommande.equals("exit")) {
      if (command.aSecondWord()) {
        System.out.println("Exit what ?");
      } else {
        return true;
      }
    }

    /* Return */

    else if (motCommande.equals("return")){
      if (previousRoom())
        System.out.println(currentRoom.longDescription());
      else
        System.out.println("Error, no previous room found");
    }

    /* Argent */

    else if (motCommande.equals("money")) {
      this.player.showMoney();
      return false;
    }

    /* bag */

    else if (motCommande.equals("bag")) {
      this.player.listBag();
      return false;
    }

    /* take */

    else if (motCommande.equals("take")) {
      takeItem(command);
      return false;
    }

    /* drop */

    else if (motCommande.equals("drop")) {
      dropItem(command);
      return false;
    }

    /* ls */

    else if(motCommande.equals("ls")) {
      currentRoom.listObject();
    }

    /* info */

    else if(motCommande.equals("info")) {
      System.out.println(player.toString());
    }

    /* current */

    else if (motCommande.equals("current")) {
      System.out.println(currentRoom.toString());
    }

    /* mission */

    else if (motCommande.equals("mission")) {
      System.out.println("The current goal is to collect the exact weight of"
      +" object wich is " + win + " kg");
    }

    /* deposite */
    else if(motCommande.equals("deposite")) {
      return checkWinLoose();
    }

    else if (motCommande.equals("open")) {
      open(command);
    }

    else if (motCommande.equals("close")) {
      close(command);
    }

    else if (motCommande.equals("wait")) {
      attendre(true);
      //goToNextRoomAI();
      currentRoom.exitString();
      return false;
    }

    else if (motCommande.equals("show")) {
      if ((command.aSecondWord()) && (command.aThirdWord()))
        if (command.getSecondWord().equals("navigo")) {
          if (command.getThirdWord().equals("SNCF"))
            return showNavigo(this.player, currentRoom);
          else {
            System.out.println("Erreur, présenter le navigo à qui ?" +
            "(seulement controleur \"RATP\")");
          }
          return false;
        }
      else {
        return showNavigo();
      }
      else {
        System.out.println("Montrer quoi (Seulement navigo) ?");
        return false;
      }
    }

    else if (motCommande.equals("valider")) {
      validerNavigo(player, currentRoom);
      return false;
    }
    return false;
  }


  /**
   * Print out some help information. Here we print some stupid, cryptic
   * message and a list of the command words.
   */
  /*@
    @ pure
    @*/
  public void printHelp() {
    System.out.println("Quests are not developped yet");
    System.out.println("However, a basic goal is to have the exact weight of"
    +" your bag by collecting some random object all over the campus\n" +
    "Please type mission to check the current objective.\n\n");
    System.out.println("Known commands are:");
    parser.showCommands();
  }


  /**
   *  Try to go to one direction. If there is an exit, enter the new room,
   * otherwise print an error message.
   * Also, the path room must be open. This is checked by calling getDoor.
   * If the getDoor is true, the room is open and can be accessed.
   * Overwise, It has to be openned by the player with the according commands.
   *
   * @param  command  commands wich the second word contain the direction
   */
  /*@
    @ requires commands != null;
    @*/
  public void goRoom(Command command) {
    if (!command.aSecondWord()) {
      // If there's no second word, we dont know where to go
      System.out.println("Go where ?");
      return;
    }

    String direction = command.getSecondWord();

    // Try going into the next room
    Room nextRoom = currentRoom.nextRoom(direction);

    if (nextRoom == null)
      System.out.println("There is no path this way!");
    else if(nextRoom.shortDescription().equals("teleportation"))
      currentRoom = randomizerRoom(roomList);
    else if (!nextRoom.getDoor())
      System.out.println("Unable, the room is closed, try with 'open' 'direction'");
    else {
      lastRoom = currentRoom;
      currentRoom = nextRoom;
      if (currentRoom.equals(villetaneuseUniversite) ||
          currentRoom.equals(drancyAvenir) ||
          currentRoom.equals(leBourget)) {
        System.out.println("N'oubliez pas de valider le navigo");
      }
    }
    System.out.println(currentRoom.longDescription());
  }

  /**
   * Undo the last move, by switching currentRoom to previousRoom
   * If it's successful, return true and set the previous to null, overwise do
   * nothing.
   *
   * This methods can only be called by aa previousRoom command attemps.
   *
   * @return boolean
   */
  /*@
    @  lastRoom == null ==> \result = false;
    @  lastRoom != null ==> \result = true;
    @  ensures currentRoom = lastRoom;
    @  ensures lastRoom = null;
    @*/
  public  boolean previousRoom() {
    if (lastRoom == null)
      return false;
    else {
      currentRoom = lastRoom;
      lastRoom = null;
      return true;
    }
  }

  /**
   * Try to open a door according to the current room
   *
   * @param command A direction commands
   */
  /*@
    @ pure
    @*/
  public void open(Command command) {
    if (!command.aSecondWord()) {
      // If there's no second word, we dont know where to go
      System.out.println("Open wich direction?");
      return;
    }

    String direction = command.getSecondWord();

    // Try going into the next room
    Room nextRoom = currentRoom.nextRoom(direction);
    if (nextRoom == null) {
      System.out.println("There is no path this way!");
      return;
    }
    System.out.println(nextRoom.openDoor());
  }

  /**
    * Try to close a door according to the current room
    *
    * @param command A direction commands
    */
  /*@
    @ pure
    @*/
  public void close(Command command) {
    if (!command.aSecondWord()) {
      // If there's no second word, we dont know where to go
      System.out.println("Close wich direction?");
      return;
    }

    String direction = command.getSecondWord();

    // Try going into the next room
    Room nextRoom = currentRoom.nextRoom(direction);
    if (nextRoom == null) {
      System.out.println("There is no path this way!");
      return;
    }
    System.out.println(nextRoom.closeDoor());
  }

  /**
   * Take an Item from the current room.
   * This method should contains a second word command. Otherwise, we should
   * exit the methode without touching anything. so it's @pure at this case.
   *
   * The other case wich is a found ObjetZork is to retrive it from the current
   * Room.
   *
   * The not found item return just an Error message and exit normally the
   * methode.
   *
   * @param    c  A parsed command line
   */
  /*@
    @ \result =
    @ c.getSecondMot => !(null)
    @ || !o.equals(null)
    @ || !(nameInArray.getSecondMot(),-1).equals(null));
    @*/
  public void takeItem(Command c) {
    ObjetZork o = nameInArray(c.getSecondWord(), 1);

    if (!c.aSecondWord()) {
      System.out.println("Missing object argument");
      return;
    }
    if (o != null) {
      retriveItem(o);
    }
    else {
      System.out.println("Error : Object not found");
    }
  }

  /**
   * Check if the name match to a object, if yes, we just return the object
   * @param  o   The name of the object
   * @param  b   boolean statement for getting either bag object, or room object
   * @return     A matching object
   */
  /*@
    @ requires o != null
    @ requires b != null
    @ ensures \result == ObjectZork
    @ pure
    @*/
  public ObjetZork nameInArray(String o, int b) {
    int size;
    int i = -1;

    if (b == 1) {
      size = this.currentRoom.getSize();
      while (++i < size) {
        if (currentRoom.getObject(i).getName().equals(o)) {
          return this.currentRoom.getObject(i);
        }
        continue;
      }
      return null;
    }
    else {
      size = this.player.getSize();
      while (++i < size) {
        if (this.player.getObject(i).getName().equals(o)) {
          return this.player.getObject(i);
        }
        continue;
      }
      return null;
    }
  }

  /**
   * Retrive a carryable item from the room to the bag
   * Added security in case of String bug
   * @param  o A Zork Object to put in the bag
   */
  /*@
    @ pure
    @*/
  public void retriveItem(ObjetZork o) {
    int size = this.currentRoom.getSize();
    int i = -1;
    while (++i < size) {
      if (this.currentRoom.getObject(i).equals(o) == true) {
        if (this.player.isAdd(o) == true) {
          this.currentRoom.remove(o);
          System.out.println("You have put the following object into your bag : "
            + o.getName());
          return;
        }
        else {
          System.out.println("Error, this object cannot be carried");
          return;
        }
      }
    }
    System.out.println("This object is not at this room");
    return;
  }

  /**
   * Deposite a ObjectZork from the bag to the room
   * @param o A ObjectZork Item
   */
  /*@
    @ requires o != null
    @ pure
    @*/
  public void depositeItem(ObjetZork o) {
    int size = player.getMACOP();
    int i = -1;

    while (++i < size) {
      if (this.player.getObject(i).equals(o)) {
        this.player.remove(o);
        this.currentRoom.add(o);
        System.out.println("Dropped item : " + o.getName());
        return;
      }
      else {
        continue;
      }
    }
  }

  /**
   * Drop a item to the room, after checking that item is in the bag
   * @param  c Name of the item
   */
  /*@
    @ requires c != null
    @ pure
    @*/
  public void dropItem(Command c) {
    ObjetZork o = nameInArray(c.getSecondWord(), 2);
    if(!c.aSecondWord()) {
      System.out.println("Drop wich item?");
      return;
    }
    if(o != null) {
      depositeItem(o);
    }
    else {
      System.out.println("Item not found in the bag");
    }
  }

   /**
  * Check if the "Check" command is valide for winning or nah.
  * If true and the counter is below 3, so we win
  * If false and counter below 3, warning message for the exact weight of bag
  * If false and over 3, game over
  * @return a boolean statemnt
  */
  /*@
    @ ensures \result == (player.get(0).getWeight() == win) && counter < 3;
    @ ensures \result != (player.get(0).getWeight() == win)
    @ pure
    @*/
  public boolean checkWinLoose() {
    if (currentRoom.shortDescription().equals("secretary") && counter < 4) {
      if (player.getCWCOP() == win && player.contains(carteEtudiante)) {
        System.out.print("You have successfully dropped the bag to the secretary");
        System.out.println( win + " kg, you have now finished the game");
        this.winGame = true;
        return true;
      }
      else {
        if (counter < 3) {
          System.out.print("You need to get exactly " + win + " kg in");
          System.out.println(" your bag");
          System.out.println("You need your Student card :");
          if (player.contains(carteEtudiante))
            System.out.println("Yes you do have it");
          else
            System.out.println("No, you don't have it");
          counter++;
          System.out.println("This is your " +counter+ " attempt");
          return false;
        }
        else {
          System.out.print("You pissed off her " + counter + " times");
          System.out.println(". You are fired. End of the game."+
          " How are you gonna buy foods now?");
          return true;
        }
      }
    }
    else {
      System.out.print("You need to be at the secretary to use that ");
      System.out.println("command");
    }
    System.out.println(currentRoom.longDescription());
    return false;
  }

  	/**
   * Random calculator between max and min
   * @param  min           A minimum integer
   * @param  max           A maximum integer
   * @return               A Randomized integer between min and max
   */
  /*@
    @ requires int min != null && max != null
    @ ensures \result == (int)(Math.random() * range) + min;
    @ pure
    @*/
  int randomWithRange(int min, int max) {
    int range = (max - min) + 1;
    return (int)(Math.random() * range) + min;
   }

   /**
  * Randomly insert an object, Randomly x times for each room
  * @param roomList An ArrayList of pieceArray
  * @param objectList   An ArrayList of ObjectZork
  */
 /*@ requires pieceArray != null
   @ requires defaultO != null
   @ pure
   @*/
 public void randomPieceObjectInsert(ArrayList<Room> roomList,
                                     ArrayList<ObjetZork> objectList) {
   int i = -1;
   int j = -1;
   int ranObjSize = randomWithRange(1,11);
   int trueSizePiece = roomList.size();
   ObjetZork o;

   while (++i < trueSizePiece - 1) {
     while (++j < ranObjSize) {
       o = randomizerObject(objectList);
       roomList.get(i).add(o);
     }
     ranObjSize = randomWithRange(1,11);
     j = -1;
     continue;
   }
 }

 /**
  * Return the calculus of a randomized object
  * @param   objectList    ArrayList of all ObjectZork
  * @return                A randomized ObjectZork
  */
 /*@ requires defaultO != null
   @ ensures \result == ObjectZorku
   @ pure
   @*/
 public ObjetZork randomizerObject (ArrayList<ObjetZork> objectList) {
   return objectList.get(randomWithRange(0, 17));
 }

 /**
  * Room randomizer
  * @param  roomList [description]
  * @return          [description]
  */
 public Room randomizerRoom (ArrayList<Room> roomList) {
   return roomList.get(randomWithRange(0, roomList.size()-4));
 }


  /**
   * Validation card transportation into the tram
   * @author Rayan Fraj
   * @param j Un joueur
   * @param p La piece actuelle
   */
  /*@
    @ requires j != null && p != null
    @ pure
    @*/
  public void validerNavigo(Player j, Room p) {
    if (currentRoom.equals(villetaneuseUniversite) ||
        currentRoom.equals(drancyAvenir) ||
        currentRoom.equals(leBourget)) {
      j.getNavigo().setValidation(true);
      System.out.println("Passe navigo validé, bon voyage");
      return;
    }
    System.out.println("Vous devez être dans une gare pour valider");
    return;
  }

  /**
   * Show card transportation to the card controller
   * @author Rayan Fraj
   * @param  j             A player
   * @param  p             Current Piece
   * @return               A boolean statement
   */
  /*@
    @ requires j != null && piece != null
    @ ensures controle = {-1,0,1}
    @*/
  public boolean showNavigo(Player j, Room p) {
    int controle = p.getConntrolAI().isValidated(j);
    checkControl();
    if (controle == 3) {
      return true;
    }

    return false;
  }

  /**
   * Secondary methods for no player and no room
   * @return   false for the while loop game.
   */
  public boolean showNavigo() {
      System.out.println("Merci de présenter votre titre de transport SVP !");
      return false;
  }


  /**
   * Permet juste le dÃ©placement mobile du tramway
   * @param show Boolean Statement, true if the guy is on the platforme, else
   * otherwise.
   */
  /*@
    @ pure
    @*/
  public void attendre(boolean show) {
    if (tt.getExits().containsValue(villetaneuseUniversite)) {
      lastTT = villetaneuseUniversite;
      tt.setExits(leBourget, null, null, null);
      villetaneuseUniversite.setExits(corridorGalilee, null, null, null);
      leBourget.setExits(maison, null, tt, null);
      currentTT = leBourget;
    }
    else if (tt.getExits().containsValue(leBourget)) {
      lastTT = leBourget;
      tt.setExits(drancyAvenir, null, null, null);
      drancyAvenir.setExits(illustration, null,   tt, null);
      leBourget.setExits(maison, null, null, null);
      currentTT = drancyAvenir;
    }
    else if (tt.getExits().containsValue(drancyAvenir)) {
      lastTT = drancyAvenir;
      tt.setExits(villetaneuseUniversite, null, null, null);
      drancyAvenir.setExits(illustration, null,   null, null);
      villetaneuseUniversite.setExits(corridorGalilee, null, tt, null);
      currentTT = villetaneuseUniversite;
    }
    if (show) {
      System.out.println("Le tramway était à " + lastTT.getName() + " il est"+
      " maintenant à " + currentTT.getName());
      System.out.println(currentRoom.exitString());
    }
  }

  /**
   * [ControlOfficerMover description]
   */
  public void ControlOfficerMover() {
    if(tt.containsControl()) {
      tt.removeControl();
      tt.nextRoom("north").addControl(controleur);
      return;
    }
    if (villetaneuseUniversite.containsControl()) {
      if (villetaneuseUniversite.nextRoom("south") != null)
        villetaneuseUniversite.removeControl();
        tt.addControl(controleur);
      return;
    }
    if (drancyAvenir.containsControl()) {
      if (drancyAvenir.nextRoom("south") != null)
        drancyAvenir.removeControl();
        tt.addControl(controleur);
      return;
    }
    if (leBourget.containsControl()) {
      if (leBourget.nextRoom("south") != null)
        leBourget.removeControl();
        tt.addControl(controleur);
      return;
    }
  }

  /**
   * AntiSpam checl protection for the controleur
   */
  public void checkControl() {
    if (controleur.getChecked()) {
      if (currentRoom.equals(tt))
        return;
      else
        controleur.setChecked();
    }
  }
}
