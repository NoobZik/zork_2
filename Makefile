# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/04 17:22:36 by NoobZik           #+#    #+#              #
#    Updated: 2018/10/16 21:01:25 by NoobZik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
CC= javac

CFLAGS= -Xlint -Werror

all:
		$(CC) $(CFLAGS) *.java

clean:
		rm -rf *.class

clean-doc:
		rm -rf docZork/

doc:
		javadoc -encoding utf8 -docencoding utf8 -charset utf8 -html5 -d docZork *.java
run:
		java Abysmal
