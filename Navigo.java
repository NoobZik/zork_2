/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Navigo.java                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/29 15:24:08 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 20:00:18 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * <p> Navigo </p>
 *
 * <p>Cette objet à part représente un titre de transport en Ile-de-France.
 * Il comporte un booléen qui permet de savoir si ce passe est validé ou non</p>
 *
 * @author Rayan Fraj
 */

public class Navigo {
  private boolean   validation;
  //Map<Tram,Room>History;
  //M
  public Navigo() {
    this.validation = false;
  }

  /**
   * Set validation according to the given value in v
   * @param v boolean value, true for validated, false overwise
   */
  /*@
    @ requires v == false || v == true;
    @ ensures this.getValidation() == v;
    @*/
  public void setValidation(boolean v) {
    this.validation = v;
  }

  /**
   * Retourne la valeur booléene du pass navigo
   * @return boolean
   */
  /*@
    @ pure
    @*/
  public boolean getValidation() {
    return this.validation;
  }
}
