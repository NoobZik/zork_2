/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ObjetZork.java                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/29 15:24:08 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 20:00:18 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.Objects;

/**
 * Definition of a single ObjectZork
 * Contains two String for name and description, one boolean carryable or not
 * and one integer for the weight in kgs.
 *
 * All varibles are final, once setted, it cannot be edited any further
 *
 * @author  Rayan Fraj
 * @author  Rakib Hernandez Sheikh NoobZik
 * @version v0.1
 * @since   382327796f2af255a7be07b1dceb50bae12f5c48
 */
public class ObjetZork {
  private String  name;
  private String  description;
  private int     weight;
  private boolean carry;

  /**
   * Object Zork default constructor
   * @param n Name String
   * @param d description String
   * @param w Weight interger in kgs
   * @param c Carryable (boolean)
   */
  /*@
    @ requires !(n.equals(null));
    @ requires !(d == null);
    @ requires !(c == false || c == true);
    @ requires (w >= 0);
    @ ensures this.getName().equals(n);
    @ ensures this.getDescription().equals(d);
    @ ensures this.getWeight() == w;
    @ ensures this.getCarry() == c;
    @ ConstructorDeclaration ;
    @ invariant n;
    @ invariant d;
    @ invariant w;
    @ invariant c;
    @*/
  public ObjetZork(String n, String d, int w, boolean c) {
    this.name = n;
    this.description = d;
    this.weight = w;
    this.carry = c;
  }

  protected ObjetZork(String n, String d, int w) {
    this.name = n;
    this.description = d;
    this.weight = w;
    this.carry = true;
  }

  /**
   * Return the name of the object
   * @return String formatted name of the object
   */
  /*@
    @ pure
    @*/
  public  String  getName() {
    return  this.name;
  }

  /**
   * Reture the description of the current object
   * @return  String formatted of the description
   */
  /*@
    @ pure
    @*/
  public  String  getDescription() {
    return  this.description;
  }

  /**
   * Return the current weight of the object
   * @return integer formatted of the weight, suppoed to be in kgs
   */
  /*@
    @ pure
    @*/
  public  int getWeight() {
    return  this.weight;
  }

  /**
   * Return the current boolean state of the carryable object
   * @return A boolean final state
   */
  /*@
    @ pure
    @*/
  public  boolean getCarry() {
    return this.carry;
  }

  /**
   * Compare two objects according to their name, description, weight and carry
   * @return boolean, true if equals, false overwise
   */
  /*@
    @ ensures ! (o instanceof ObjetZork) ==> !\result;
    @ ensures (o instanceof ObjetZork) ==>
    @   \result == (this.getName().equals((((ObjetZork) o).getName()))
    @              && (this.getCarry() == (((ObjetZork) o).getCarry()))
    @              && (this.getWeight() == (((ObjetZork) o).getWeight()))
    @              && (this.getDescription().equals(((ObjetZork) o).getDescription())));
    @ pure
    @*/
  @Override
  public  boolean equals(Object o) {
    if(!(o instanceof ObjetZork))
      return false;

    ObjetZork oz = (ObjetZork) o;
    if (oz.getName().equals(this.getName()))
      if(oz.getCarry() == this.getCarry())
        if(oz.getWeight() == this.getWeight())
          if(oz.getDescription().equals(this.getDescription()))
          return true;
    return false;
  }

  /**
   * Customized hashcode keygen
   * @return generated hash in integer
   */
  /*@
    @ pure
    @*/
  @Override
  public  int hashCode() {
    return  Objects.hash(getName(),getCarry(),getWeight());
  }

  /**
   * Customized toString methode according to the current class
   */
  /*@
    @ pure
    @*/
  @Override
  public  String  toString() {
    return this.getName() + ", " + this.getDescription() + ", "
      + this.getWeight() + "kg, "
      + ((this.getCarry()) ? "carryable" : "not carryable");
  }

  /**
   * Another toString methode without the carryable option since it's pointless
   * to show it to the bag content.
   * @return String formatted output of bag contents
   */
  /*@
    @ pure
    @*/
  public  String  toStringBag() {
    return this.getName() + " " + this.getDescription() + " " +
     this.getWeight() + "kg";
  }
}
