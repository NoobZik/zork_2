/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Parser.java                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 21:03:47 by NoobZik           #+#    #+#             */
/*   Updated: 2018/09/30 21:03:47 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 *
 * This class is part of Zork. Zork is a simple, text based adventure game.
 *
 * This parser reads user input and tries to interpret it as a "Zork"
 * command. Every time it is called it reads a line from the terminal and
 * tries to interpret the line as a two word command. It returns the command
 * as an object of class Command.
 *
 * The parser has a set of known command words. It checks user input against
 * the known commands, and if the input is not one of the known commands, it
 * returns a command object that is marked as an unknown command
 *
 * @author     Michael Kolling
 * @author     Rakib Hernandez Sheikh NoobZik
 * @author     Rayan Fraj
 * @version    1.0
 * @since      a66dcb10b3c541d3a761da1cdcd81e55f8e48612
 */

public class Parser {

  // Hold all valid commands
  private CommandWords commands;


  /**
   *  Initialise un nouvel analyseur syntaxique
   */
  public Parser() {
    commands = new CommandWords();
  }


  /**
   *  Lit une ligne au terminal et tente de l'interpréter comme constituant une
   *  commande composée de deux mots. La commande est alors renvoyée sous forme
   *  d'une instance de la classe Commande.
   *
   * @return    La commande utilisateur sous la forme d'un objet Commande
   */
  public Command getCommand() {
    // pour mémoriser la ligne entrée par l'utilisateur
    String inputeLine = "";
    String word1;
    String word2;
    String word3;

    // affiche l'invite de commande
    System.out.print("> ");

    BufferedReader reader = new BufferedReader(new InputStreamReader(
      System.in));
    try {
      inputeLine = reader.readLine();
    } catch (java.io.IOException exc) {
      System.out.println("Une erreur est survenue pendant la lecture de votre commande: "
         + exc.getMessage());
    }

    StringTokenizer tokenizer = new StringTokenizer(inputeLine);

      // récupération du permier mot (le mot commande)
    word1 = (tokenizer.hasMoreTokens()) ? tokenizer.nextToken() : null;
    word2 = (tokenizer.hasMoreTokens()) ? tokenizer.nextToken() : null;
    word3 = (tokenizer.hasMoreTokens()) ? tokenizer.nextToken() : null;


    // note: le reste de la ligne est ignoré.

    // Teste si le permier mot est une commande valide, si ce n'est pas
    // le cas l'objet renvoyé l'indique
    if (commands.isCommand(word1)) {
      return new Command(word1, word2, word3);
    } else {
      return new Command(null, word2, word3);
    }
  }


  /**
   *  Print out a list of valid command words.
   */
  public void showCommands() {
    commands.showAll();
  }
}
