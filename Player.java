/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Player.java                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 17:41:56 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 23:19:22 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.ArrayList;

/**
 * A Player for the Abysmal game.
 *
 * This is where all player related info is set. A player is made of it String
 * format name, integer fomart for the amount of carried money, the CCOP,
 *  a final MCOP, a final MACOP and a list of the CCOP
 *
 * @author Rayan Fraj
 * @author Rakib Hernandez Sheikh NoobZik
 * @version 0.01
 * @since 64e9f12f03774fe0db2869ea681b06acf78a7e0b
 */

public class Player extends ArrayListContents{
  private String name;
  private int cwcop;
  final static private int mwcop = 55;
  final static private int macop = 20;
  private int money;
  private int health;
  private Navigo n;
  private Weapons attack;
  private Weapons armor;
  private ArrayList<SupportObject> support;

  /**
   * Default constructor
   * Set the name uppon the creation of the player.
   * Set the CWCOP at 0, money at 50 euroes and create a navigo instance
   *
   * @param n A String formatted name
   *
   */
  public Player(String n) {
    super();
    this.setName(n);
    this.setCWCOP(0);
    this.setMoney(50);
    this.n = new Navigo();
    this.health = 500;
    this.support = new ArrayList<SupportObject>();
  }

  /**
   * This is an methode abstraction from ArrayListContents.
   * Add an object into the superclass objects ArrayList. then update the value
   * of the CWCOP
   */
  /*@
    @ requires !(o.equals(null));
    @ ensures this.contains(o);
    @ ensures \old(cwcop+o.getWeight()) == getCWCOP();
    @*/
  public  boolean add(ObjetZork o) {
    if(this.objects.add(o)) {
      this.setCWCOP(o.getWeight());
      return true;
    }
    return false;
  }

  /**
   * Remove an object from the list. Use the negation value to substract the
   * CWCOP
   * @param o The object to remove from the superclass ArrayList
   * @return true if successfull false overwise
   */
  /*@
    @ requires !(o.equals(null));
    @ ensures !(this.contains(o)); && (\old(this.getCCOP()) == this.getCCOP()+1;)
    @*/
  public boolean remove(ObjetZork o) {
    if (this.objects.remove(o)) {
      this.setCWCOP(o.getWeight()*-1);
      return true;
    }
    return false;
  }

  /**
   * Return the name of the player
   * @return String formatted name of the player
   */
  /*@
    @ pure
    @*/
  public String getName() {
    return this.name;
  }

  /**
   * Set the name of the player
   * @param n String formatted player name
   */
  /*@
    @ requires !(n.equals(null));
    @ ensures this.name.equals(this.getName());
    @*/
  public void setName(String n) {
    this.name = n;
  }

  /**
   * Return the Current Carried Object of the Player
   * @return Integer formatted
   */
  /*@
    @ pure
    @*/
  public int getCWCOP() {
    return this.cwcop;
  }

  /**
   * Set the Current carried object of the player.
   * This is a local method, it should never be used outside this class
   * and can be only be invocated by remove or add
   * @param ccop
   */
  /*@
    @ requires ccop != 0;
    @ ensures ccop == this.ccop;
    @*/
  private void setCWCOP(int cwcop) {
    this.cwcop += cwcop;
  }
  /**
   * Return the Maximum Carryable Object of the Player
   * The "this" token is removed in order to remove the following warning
   * [Java] The static field Player.mcop should be accessed in a static way
   *
   * @return Integer formatted
   */
  /*@
    @ pure
    @*/
  public int getMWCOP() {
    return mwcop;
  }

  /**
   * Return the Maximum Amount of Carryable Object of the Player
   * The "this" token is removed in order to remove the following warning
   * [Java] The static field Player.mcop should be accessed in a static way
   *
   * @return Integer formatted
   */
  /*@
    @ pure
    @*/
  public int getMACOP() {
    return macop;
  }

  /**
   * Return the Current amount of money
   * @return Integer formatted
   */
  /*@
    @ pure
    @*/
  public int getMoney() {
    return this.money;
  }
  /**
   * Set the current ammount of money by a arithmetic operator.
   * @param m Amount of money (can be negative)
   */
  /*@
    @ requires m != 0;
    @ ensures \prev(this.money) == this.money - m
    @ || \prev(this.money) == this.money + m;
    @*/
  public void setMoney(int m) {
    this.money += m;
  }

  /**
   * Print the current amount of money of the player
   */
  /*@
    @ pure
    @*/
  public void showMoney() {
    System.out.println("Current amount of money :" + this.getMoney());
  }

  /**
   * Method that check if the selected Zork Object can be carried or nah.
   * If yes, stack it to the bag.
   * If not, just tell him nicely to get the fuck off.
   * @param o A Zork Object.
   * @return  A boolean statement for successfull insert or nah.
   */
  /*@
    @ requires o != null
    @ invariant w1 = getWeight();
    @ invariant w2 = this.weight;
    @ invariant w0 = this.maxWeight;
    @ ensures \result (* true if w2+w1 <= w0 *);
    @ ensures \result (* false if w2+w1 > 0  || getCarry == false)
    @*/
  public boolean isAdd(ObjetZork o) {
    int w0 = this.getMWCOP();
    int w1 = o.getWeight();
    int w2 = this.getCWCOP();

    if (o.getCarry() == true) {
      if (w2+w1 <= w0){
        this.add(o);
        return true;
      }
      else {
      System.out.println("Object is overweight to be carried");
      }
    }
    return false;
  }

  /**
   * print list the content of the bag.
   */
  /*@
    @ pure
    @*/
  public  void  listBag()  {
    int i = -1;
    System.out.print("Content of the bag : ");
    if(this.getSize() == 0) {
      System.out.println("None");
      return;
    }
    System.out.println("");
    while(++i < this.getSize())
      System.out.println(this.objects.get(i).toStringBag());
  }

  /**
   * Get the navigo card from the player
   * @return A Navigo type
   */
  /*@
    @ pure
    @*/
  public Navigo getNavigo() {
    return n;
  }

  /**
   * [setAttack description]
   * @param o [description]
   */
  public void setAttack(Weapons o) {
    this.attack = o;
  }

  /**
   * [getAttack description]
   * @return [description]
   */
  public Weapons getAttack() {
    return this.attack;
  }


  /**
   * [getArmor description]
   * @return [description]
   */
  public Weapons getArmor() {
    return this.armor;
  }

  /**
   * [setArmor description]
   * @param o [description]
   */
  public void setArmor(Weapons o) {
    this.armor = o;
  }
  /*
  public void receiveDamage(Weapons o) {
    int trueTaken = o.trueDamage();
    int negation = this.armor.reduceDamage(trueTaken);
    System.out.println("Damage taken : " + negation);
    this.health -= negation;
  }*/

  public void receiveDamage(int i) {
    int negation = this.armor.reduceDamage(i);
    System.out.println("Damage taken : " + negation);
    this.health -= negation;
  }

  /**
   * [dealDamage description]
   * @return [description]
   */
  public int dealDamage() {
    return attack.trueDamage();
  }


  /**
   * Customized toString methode to match with the current class.
   * Should show basic player info
   * @return String formatted player info
   */
  /*@
    @ pure
    @*/
  @Override
  public String toString() {
    return (
    "--------------INFO-------------------\n" +
    "Name : " + this.getName() +
    "\nCurrent carried objects : " + this.getSize() +
    "\nMaximum amount of carryable objects " + getMACOP()) +
    "\nCurrent amount of carried object : " + getCWCOP() + "kg" +
    "\nMaximum carryable objects : " + getMWCOP() + "kg" +
    "\nType 'bag' to print the bag content.";
  //  "\nCurrent Weapon : " + this.attack.toStringWeapon() +
  //  "\nCurrent Armor :" + this.armor.toStringWeapon();
  }
}
