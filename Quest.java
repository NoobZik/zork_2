/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Quest.java                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 23:20:58 by NoobZik           #+#    #+#             */
/*   Updated: 2018/11/14 20:36:16 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Quest
 *
 * Not used
 *
 * @author Rayan Fraj
 * @author Rakib Hernandez SHEIKH
 */
public class Quest {
  private String name;
  private String description;
  private boolean isFinished;
  private boolean statut;

  public Quest(String n, String d, boolean f, boolean s) {
    name = n;
    description = d;
    isFinished = f;
    statut = s;
  }
}
