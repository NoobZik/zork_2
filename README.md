# Rapport de projet : Abysmal #

## Membre du groupe ##

| Nom Prénom | Numéro étudiant |
|------------|-----------------|
| Sheikh Rakib | 11502605      |
| Fraj Rayan | 11607212        |

# Commentaires sur le feedback #

>**Rapport** : E
>
>*Rapport trop court pour détailler le projet.*

Réponse : Le rapport sera allongé en conséquence.

>**Code**: E
>
>*Utilisation d'autres noms de class. Il n'y pas les classe piece, ObjetZork.*

Réponse : Ok, donc ça fait doit faire partie du barème.
Au moins mon code il compile sans erreurs, sans warnings, respect des normes, indenté, bande d'enflures !

# Description d'Abysmal #

##### (Actuelle) #####

Vous êtes en mission pour la savante banlieue. Cette version est l'entrainement. Vous devez récolter les objets qui sont éparpillé dans les différentes pieces vers votre sac et le déposer au secrétariat.

Tout d'abord, récupérez la carte étudiante qui se trouve a la maison (Gare Le Bourget).


La contrainte est la suivante : Le poids totale du sac devra être égale au poids exigé par la gardienne. Pour cela, déplacez vous dans une pièce qui contient des objets à ramasser. Ramassez les objets et essayez de faire un calcul mental pour rechercher les objets dont le poids totale est égale à celui qui est demandé.

Une fois que vous avez le poids exacte demandé, allez au secrétariat qui se trouve sur le campus de Villetaneuse (Accès par la gare Villetaneuse - Universite), north north et north.

Utiliser la commande deposite pour finir le jeu.

Remarque : vous utiliserez les transport en commun, il est impératif de valider votre titre de transport avant de montrer dans le tramway. A titre indicatif, vous commencez le jeu avec 50 euros.

(Non developpé, manque de temps). Vous devez aussi assassiner des boss qui surgissent sur les deux campus (Villetaneuse et Bobigny).

# Scénario de test #

Je ne peux pas vraiment prévoir un scénario de test pour gagner rapidement, puisque les objets sont générés aléatoirement pour chaque pièce. De plus, le joueur n'a rien dans son sac au début du jeu (sauf une carte étudiante). Il faudra un peu de recherche pour avoir le poids exacte exigé par la secrétaire.

Tout d'avoir, récupérez votre carte étudiante, south, valider, wait, wait, wait, south, wait, wait, north, north, take carteEtudiante. Ensuite commencez à chercher les objets en partant soit au campus Villetaneuse, soit au campus Bobigny.

Pour mettre un objet vers le sac, il faut faire la commande take 'l'objet'. L'action inverse est 'drop 'l'objet'.

Pour déposer le sac chez la secretaire, il faut tout simplement faire la commande 'deposite'. (Dispo au campus Villetaneuse uniquement, north, north, north).

Pour faire bouger le tramway, il faut tapper la commande wait.

La pièce de téléportation se trouve dans la maison. L'accès se fait depuis la gare de "Le Bourget", north north et north.

Si vous avez toujours pas compris, relisez le paragraphe merde alors!

En revanche, un scénario rapide pour perdre est d'aller au secrétariat et de
taper :

```bash
-> deposite
```
3 fois d'affilé.

Ou alors, on peut monter dans le tramway sans valider, saisir show navigo sncf, sortir du tramway (north) pour re-rentrer et re-saisir show navigo sncf.

# Détails des commandes (Version très longue) #

1.  Vous en avez déjà marre de jouer ? Ce jeu est trop casse-tête ? Enfin bref, on ne va pas vous énumerer les questions que vous vous posez. Pour cela vous avez une commande pour sortir du jeu.

```bash
-> exit
```

#### 1.  Ouverture/Fermeture des salle. ####

Pendant votre aventure, il y a de très fortes chances de vous retrouver face à une Pièce qui est fermé. Le jeu ferme les portes des pièces au fur et a mesure du temps et les missions qui vous ont été assignées requièrent que certaines pièces soit ouverte. Vous avez donc a votre disposition une commande qui permet l'ouverture des pièces.

```bash
-> open
```

Vous avez également une commande qui permet la fermuture de cette pièce, les circonstances actuel du jeu font que l'utilisation de la fermeture d'une pièce est négligable

```bash
-> close
```

#### 2.  Se déplacer dans le jeu ####

Vous avez la possibilité de vous déplacer entre plusieurs pièces qui est :

```bash
-> go
```

Le déplacement d'une pièce à l'autre se repose essentiellement sur un déplacement d'un point cardinal. Cette commande doit donc prendre en argument (en anglais) un point cardinal (Par souci de simplicité, on se restreint au quatres point cardinaux basique).

Vous avez également la possibilité de retourner sur la pièce précédente, à tout moment, à condition d'avoir fait un déplacement. La limite du retour à la pièce précédente est de 1 (autrement dit, on ne peut pas faire plus de 1 retour). La commande qui fait cette action est la suivante :

```bash
-> return
```

#### 3.  Affichage des informations relative au jeu ####

Il est possible d'afficher une information générale concernant le joueur, il suffit de saisir "info" pour que le jeu vous y affiche.

```bash
-> info
```

Vous avez la possibilité de consulter la liste des objets présente dans la pièce, cette commande est matérialisé par

```bash
-> ls
```

En ce qui concerne la liste des objets transportés par le joueur, il suffit d'utiliser la commande "bag". Cette commande liste tout simplement le contenue du sac du joueur.

```bash
-> bag
```

Pour connaitre la quantité d'argent possédé :

```bash
-> money
```

Si jamais vous vous sentez perdu, la commande suivant vous rappel la pièce actuel où vous vous trouvez avec ses différentes sorties.

```bash
-> current
```

Pour connaitre la mission en cours pour cette partie :

```bash
-> mission
```


#### 4.  Interraction utilisateur et jeu ####

Vous serez ammener à vouloir prendre des objet qui sont dans la pièce courrante vers votre sac, pour cela il suffit d'utiliser la commande 'take' qui va effectuer cette action.

```bash
-> take
```

La commande drop permet de faire l'action inverse, c'est-à-dire
```bash
-> drop
```

Lorque vous avez finalement le poids exacte demandé, allez au secrétariat pour déposer le sac, elle se trouve au campus Villetaneuse, north, north, north.
La commande suivante permet de faire cette action de dépot de sac.
```bash
-> deposite
```

Lorsque vous vous retrouvez dans l'une des gare suivante : Villetaneuse Université, Drancy Avenir, Le Bourget. Vous devez valider le titre de transport avant de monter dans le tramway. La commande suivante permet de faire cette action

```bash
> valider
```

Il se peut qu'il y a pas tramway dans la gare où vous vous trouvez. Il faudra tout simplement patienter avec la commande suivante :

```bash
-> wait
```
Si jamais un controleur de billet vous demande de présenter votre titre de transport, utilisez la commande suivante :

```bash
-> show navigo SNCF
```

# Ammendement #

*   La carte à été agrandie pour accueillir le campus de Bobigny, la maison. 3 Nouvelles gares sont disponible à savoir : Villetaneuse Université, Le Bourget et Drancy Avenir.
*   Introduction d'un tramway pour relier les trois gares.
*   Ajout des méthods concernant l'ouverture et la fermeture des portes.
*   Ajout d'une méthode qui renvoie une pièce aléatoire (Préparation téléportation)
*   La liste de pièce sont maintenant en tant que variable globale
*   Implémentation du titre de transport et méthodes associés.
*   Ajout préparatif des armes, objets de support
*   Implémentation du Controleur de billet (PNJ)
*   Téléportation
*   Le parser comprends maintenant une commande sous 3 mots.
*   Fix pour la mise-à-jour des sorties disponibles
*   Ajout de nouvelles commandes "show", "valider", "open", "close", "wait"

Les classes suivante : Weapons, Quests, SupportObject. sont implémenté mais ne sont pas utilisé dans le jeu (Manque de temps).

Pour la classe Weapons : Cette classe en extension avec ObjetZork devait matérialisé les armes dans le but de combattres les bosses du jeu.
Pour la classe SupportObject : Cette classe en extension avec ObjetZork devait matérialisé les objets de soin ou de buff qui seront uniquement utilisable en combat contre des bosses.
Pour la classe Quests : Cette classe permet de définir une quête sur comment le finir, les actions attendu, etc, etc...

En thoérie, il n'y a pas d'erreurs de compilation, ni d'averstissment. Il ne devrait pas avoir des bugs qui cassent le fonctionnement du jeu.
Les seuls bugs qui peuvent survenir sont les bugs de mauvais comportement du jeu.

### Répartition des tâches ###

On va faire simple. Tout ce qui n'est pas mentionné est fait par Rayan.
Ce que j'ai fait mais que je n'ai pas encore mentionné :
*   Mise en héritage de Room et Player
*   Ajouts des commandes, listes de rooms (carte)
*   Weapons, SupportObject, Quest. (Non finalisé, manque de temps suite à un emploi à temps plein)

Sinon si vous êtes en galère à trouver ce qu'il a fait : Player, ObjetZork, AIPlayer, ControlOfficer

**Bilan : Bon maintenant go retourner sur des projets interresante pour moi.**

# Listes de commandes #

| Commandes                        | Description |
|:---------------------------------|:-------------|
| go (north/south/west/east)       | Aller dans une piece dans la direction choisie |
| exit                             | Quitte le jeu |
| return                           | Retourne vers la pièce précédente |
| money                            | Affiche l'argent actuelle du joueur |
| bag                              | Liste les objets présent dans le sac |
| take (objet)                     | Prends un objet dans la pièce vers le sac |
| drop (objet)                     | Prends un objet dans le sac vers la pièce |
| ls                               | Liste les objets présent dans la pièce |
| info                             | Affiche les info du joueur actuelle |
| current                          | Affiche les info de la pièce actuelle |
| mission                          | Affiche la mission actuelle |
| deposite                         | Dépose le sac au secretariat |
| show navigo SNCF                 | Montrer son Navigo au PNJ |
| open (north/south/west/east)     | Ouvre une porte |
| close (north/south/west/east)    | Ferme une porte |
| wait                             | Passer le tour, faire patienter |
| valider                          | Valide son titre de transport (en gare uniquement) |

# Listes des Objets #

| Nom de la pièce  | Transportable | Poids  |
| :--------------  | :-------------| :----- |
| Micro            | Oui           | 1 kg   |
| Retro-Projecteur | Oui           | 5 kg   |
| Tableau          | Non           | 30 kg  |
| diskPoids        | Oui           | 50 kg  |
| Cours (De PACES) | Oui           | 10 kg  |
| Chaise           | Non           | 900 kg |
| Voitures         | Non           | 900 kg |
| Haribo           | Oui           | 2 kg   |
| Velleda          | Oui           | 20 kg  |
| Cannette1        | Oui           | 2 kg   |
| Cannette2        | Oui           | 2 kg   |
| Livre            | Oui           | 2 kg   |
| Table            | Non           | 20 kg  |
| Katana           | Oui           | 20 kg  |
| PC               | Oui           | 10 kg  |
| EcranPC          | Oui           | 5 kg   |
| SourisPC         | Oui           | 2 kg   |
| ClavierPC        | Oui           | 2 kg   |
| carteEtudiante   | Oui           | 0 kg   |

# Carte #

![Carte](markdown/projetV2Abysmal.png)
