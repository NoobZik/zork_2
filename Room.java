/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Room.java                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/30 20:01:34 by NoobZik           #+#    #+#             */
/*   Updated: 2018/10/28 23:21:33 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;

/**
 *  Class Room - a room in an adventure game. <p>
 *
 *  This class is part of Zork. Zork is a simple, text based adventure game.
 * </p> <p>
 *
 * "Room" represents one location in the scenery of the game.  It is
 * connected to at most four other rooms via exits.  The exits are labelled
 * north, east, south, west.  For each direction, the room stores a reference
 * to the neighbouring room, or null if there is no exit in that direction.</p>
 *
 * @author     Michael Kolling
 * @author     Rakib Hernandez Sheikh NoobZik
 * @author     Rayan Fraj
 * @version    1.1
 * @since      d8d1f6bbca12b2c304e286f59ca334338585edb9
 */

public class Room extends ArrayListContents{
  private String description;
  private Map<String,Room> exits;
  private boolean openable;
  private boolean door;
  private AIplayer currentAI;
  private ControlOfficer currentCOAI;

  /**
   * Create a room described "description". Initially, it has no exits.
   * "description" is something like "a kitchen" or "an open court yard".
   *
   * @param  description          Room description.
   * @param  l                    Array of zork objects.
   */
  /*@
    @ requires !(description.equals(null));
    @ requires (l.size() >= 0);
    @ ensures this.description.equals(description);
    @ invariant description;
    @ invariant exits
    @ invariant openable;
    @ invariant door;
    @*/
  public Room(String description, ArrayList<ObjetZork> l) {
    super(l);
    this.description = description;
    exits = new HashMap<String,Room>();
    openable = false;
    door = false;
  }

  /**
   * Create a room described "description". Initially, it has no exits.
   * "description" is something like "a kitchen" or "an open court yard".
   *
   * @param  description  Room description.
   */
  /*@
    @ requires !(description.equals(null));
    @ ensures this.description.equals(description);
    @ invariant description;
    @ invariant exits;
    @ invariant openable;
    @ invariant door
    @*/
  public Room(String description) {
    super();
    this.description = description;
    exits = new HashMap<String,Room>();
    openable = false;
    door = true;
  }

  /**
   * Create a room described "description" with a openable room.
   * Initially, it has no exits. "description" is something like "a kitchen" or
   * "an open court yard".
   *
   * @param  description  Room description.
   * @param  o Boolean statement for a door-room or no-door-room
   */
  /*@
    @ requires !(description.equals(null));
    @ ensures this.description.equals(description);
    @ invariant description;
    @ invariant exits;
    @ invariant openable;
    @ invariant door;
    @*/
  public Room(String description, boolean o) {
    super();
    this.description = description;
    exits = new HashMap<String,Room>();
    openable = true;
    door = o;
  }

  /**
   * Define the exits of this room.  Every direction either leads to
   * another room or is null (no exit there).
   *
   * @param  north   North exits
   * @param  west    West exits
   * @param  south   South exits
   * @param  east    East exits
   */
  /*@
    @  requires (north   != null);
    @  requires (west    != null);
    @  requires (east  != null);
    @  requires (south    != null);
    @*/
  public void setExits(Room north, Room west, Room south, Room east) {
    exits.clear();
    if (north != null)
      exits.put("north", north);
    if (west != null)
      exits.put("west", west);
    if (south != null)
      exits.put("south", south);
    if (east != null)
      exits.put("east", east);
  }
  /**
   * [getExits description]
   */
  /*@
    @ pure
    @*/
  public Map<String,Room> getExits() {
    return exits;
  }


  /**
   *  Return the description of the room (the one that was defined in the
   * constructor).
   *
   * @return    String room description
   */
  /*@
    @ pure
    @*/
  public String shortDescription() {
    return description;
  }


  /**
   * Return a long description of this room, on the form:
   *   Room: kitchen
   *   You are in the kitchen.
   *   Exits: north west
   *   Items: none
   *
   * @return    Room description String
   */
  /*@
    @ pure
    @*/
  public String longDescription() {
    return "You are in " + description + ".\n" + exitString();
  }


  /**
   * Return a string describing the room's exits, for example
   * "Exits: north west ".
   *
   * @return    Exits description of the room
   */
  /*@
    @ pure
    @*/
  public String exitString() {
    String resulString = "Exits : ";
    Set<String> keys = exits.keySet();
    for (Iterator<String> iter = keys.iterator(); iter.hasNext(); ) {
      resulString += " " + iter.next();
    }
    return resulString;
  }


  /**
   * Return the room that is reached if we go from this room in direction
   * "direction". If there is no room in that direction, return null.
   *
   * @param  direction  Direction taken
   * @return            Room if it does exists, null overwise
   */
  /*@
    @ requires !(direction.equals(null));
    @*/
  public Room nextRoom(String direction) {
    return exits.get(direction);
  }

  /**
   *
   * Remove an object from the ArrayList of the room
   *
   * @param o The object to remove from the arraylist
   * @return boolean statement if it's successful or not
   */
  /*@
    @  requires !(o.equals.(null));
    @  ensures !(objects.contains(o));
    @*/
  public  boolean remove(ObjetZork o) {
    return this.objects.remove(o);
  }

  /**
   *
   * Remove an object from the ArrayList of the room
   *
   * @param o The object to remove from the arraylist
   * @return boolean statement if it's successful or not
   */
  /*@
    @  requires !(o.equals.(null));
    @  ensures objects.get(o).equals(o);
    @*/
  public  boolean add(ObjetZork o) {
    return this.objects.add(o);
  }

  /**
   * Print the content of the ArrayList Objects (Room)
   */
  /*@
    @ pure
    @*/
  public  void listObject() {
    int i = -1;
    System.out.print("Content of the Room :");
    if(this.getSize() == 0) {
      System.out.println(" None");
      return;
    }
    System.out.println();
    while(++i < getSize()) {
      System.out.println(objects.get(i).toString());
    }
  }

  /**
   * Get the value of openable, intend to check if this is a room or a corridor
   * @return boolean
   */
  /*@
    @ pure
    @*/
  public boolean getOpenable() {
    return openable;
  }

  /**
   * Get the statut of the door, should be only if openable is true
   * @return Boolean true if the door is open, false overwise.
   */
  public boolean getDoor() {
    return door;
  }

  /**
   * Try to open a room. We need to check first if the room is opennable. Then
   * we change the state of the door lock if it's locked, overwise nothing is
   * touched
   * @return String formatted
   */
  /*@
    @ pure
    @*/
  public String openDoor() {
    if (!this.getOpenable())
      return "This room have no lock";
    if (!this.getDoor()) {
      this.setDoor();
      return "This room is now open";
    }
    else {
      return "This room is already openned";
    }
  }

  /**
   * Try to close a room. We need to check first if the room is opennable. Then
   * we change the state of the door lock if it's locked, overwise nothing is
   * touched
   * @return String formatted
   */
  /*@
    @ pure
    @*/
  public String closeDoor() {
    if (!this.getOpenable())
      return "This room have no lock";
    if (this.getDoor()) {
      this.setDoor();
      return "This room is now closed";
    }
    else {
      return "This room is already closed";
    }
  }

  /**
   * Set the door according to the previous state
   */
  /*@
    @ ensures \result = ()(true == door) ==> false) ^ ((false == door) = false)
    @*/
  protected void setDoor() {
    door = (door) ? false : true;
  }


  /* Tramway operation functions */

  /**
   * Get the current AI in the roome
   * @return AIplayer
   */
  public AIplayer getAI() {
    return currentAI;
  }

  /**
   * Get the controler
   * @return A control officer
   */
  public ControlOfficer getConntrolAI() {
    return currentCOAI;
  }

  /**
   * Return a boolean statement about the controler being here or not
   * @return boolean statement
   */
  public boolean containsControl() {
    return (currentCOAI != null) ? true : false;
  }

  /**
   * Add the controler
   * @param c A controler
   */
  public void addControl(ControlOfficer c) {
    this.currentCOAI = c;
  }

  /**
   * Remove the controler
   */
  public void removeControl() {
    currentCOAI = null;
  }

  /**
   * Get the name of the room
   * @return String nam formatted
   */
  /*@
    @ pures
    @*/
  public String getName() {
    return description;
  }

  /**
   * HashCode generation
   * @return Generated hashcode
   */
  @Override
  public int hashCode() {
    return Objects.hash(description);
  }

  /**
   * Equals overrides for room
   * @param  oz Object to be compared
   * @return    boolean statement
   */
  @Override
  public boolean equals(Object oz) {
    if (!(oz instanceof Room))
      return false;
    Room o = (Room) oz;
    if (o.getName().equals(this.getName()))
      return true;
    return false;
  }



  /**
   * Customized toString methods for Room class
   *
   * @return String formatted.
   */
  /*@
    @ pure
    @*/
  @Override
  public String toString() {
    return "Current room : " + description +"\n"
            + "Amount of objects : " + this.objects.size() + "\n"
            + exitString() + "\n"
            + "List of the objects available by typing 'ls'";

  }
}
