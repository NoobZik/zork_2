import java.util.Objects;

/**
 * SupportObject
 *
 * Not used
 *
 * @author Rayan Fraj
 * @author Rakib Hernandez SHEIKH
 */
public class SupportObject extends ObjetZork {
  private int healV;
  private int damageBonus;
  private int armorBonus;


  /**
   * [SupportObject description]
   * @param n  [description]
   * @param de [description]
   * @param h  [description]
   * @param d  [description]
   * @param a  [description]
   */
  public SupportObject (String n, String de, int h, int d, int a) {
    super(n, de, 0);
    healV = h;
    damageBonus = d;
    armorBonus = a;
  }


  /**
   * [getHealV description]
   * @return [description]
   */
  public int getHealV() {
    return healV;
  }

  /**
   * [getDamageBonus description]
   * @return [description]
   */
  public int getDamageBonus () {
    return damageBonus;
  }

  /**
   * [getArmorBonus description]
   * @return [description]
   */
  public int getArmorBonus () {
    return armorBonus;
  }

  /**
   * [hashCode description]
   * @return [description]
   */
  @Override
  public int hashCode() {
    return super.hashCode() + Objects.hash(healV,damageBonus, armorBonus);
  }

  /**
   * [equals description]
   * @param  oz [description]
   * @return    [description]
   */
  @Override
  public boolean equals (Object oz) {
    if (!super.equals(oz))
      return false;
    if (oz instanceof SupportObject) {
      SupportObject o = (SupportObject) oz;
      if (this.healV == o.getHealV())
        if (this.getDamageBonus() == o.getDamageBonus())
          if (this.getArmorBonus() == o.getArmorBonus())
            return true;
    }
    return false;
  }

  /**
   * [toString description]
   * @return [description]
   */
  @Override
  public String toString() {
    return getName() + " " + getDescription() + "\n Instant Health Regen :" +
            getHealV() + "\n Armor bonus :" + getArmorBonus() +
            "\n Damage Bonus :" + getDamageBonus();
  }
}
