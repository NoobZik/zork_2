import java.util.Objects;
/**
 * Weapons
 *
 * Not used
 *
 * @author Rayan Fraj
 * @author Rakib Hernandez SHEIKH
 */
public class Weapons extends ObjetZork {
  private int damage;
  private int percentage;
  private int armor;

  public Weapons(String n, String d, int dmg, int per, int a) {
    super(n,d,0);
    this.damage = dmg;
    this.percentage = per;
    this.armor = a;
  }

  /**
   * Return the damage value of the object
   * @return int
   */
  /*@
    @ pure
    @*/
  public int getDamage() {
    return this.damage;
  }

  /**
   * Return the percentage bonus of the object
   * @return int
   */
  /*@
    @ pure
    @*/
  public int getPercentage() {
    return this.percentage;
  }

  /**
   * Return the armor value of the object
   * @return int
   */
  /*@
    @ pure
    @*/
  public int getArmor() {
    return this.armor;
  }

  /**
   * [isApplyDamagePercentage description]
   * @return [description]
   */
  private int isApplyDamagePercentage() {
    return (this.percentage != 0.0) ? this.damage * (1 + (percentage/100)) : 0;
  }

  /**
   * [trueDamage description]
   * @return [description]
   */
  public int trueDamage() {
    int i = isApplyDamagePercentage();
    return (i == 0) ? i : this.damage;
  }

  /**
   * [reduceDamage description]
   * @param  d [description]
   * @return   [description]
   */
  public int reduceDamage(int d) {
    return (d - this.armor < 0) ? 0 : d - this.armor;
  }

  /**
   * Full stat weapon description
   * @return String formatted
   */
  public String toStringWeapon() {
    return this.getName() + " " + this.getDescription() + "\nDamage :"
     + getDamage() + "\nArmor :" + getArmor() + "\n percentage :"
     + getPercentage();
  }

  /**
   * [equals description]
   * @param  o [description]
   * @return   [description]
   */
  @Override
  public  boolean equals(Object o) {
    if (!super.equals(o))
    return false;
    if(!(o instanceof Weapons))
      return false;

    Weapons oz = (Weapons) o;
    if (this.getDamage() == oz.getDamage()) {
      if (this.getPercentage() == oz.getPercentage())
        if (this.getArmor() == oz.getArmor())
          return true;
    }
    return false;
  }

  /**
   * [hashCode description]
   * @return [description]
   */
  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(),damage,percentage,armor);
  }

  /**
   * [toString description]
   * @return [description]
   */
  @Override
  public String toString() {
    return "Weapon : " + this.getName();
  }
}
